package Project5;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class JamesBondTest {

	JamesBond jBondTest = new JamesBond();
	
	@Test
	public void testregexBond0 () throws Exception{
		assertFalse(jBondTest.regexBond("("));
	}

	@Test
	public void testregexBond1 () throws Exception{
		assertFalse(jBondTest.regexBond("( ("));
	}

	@Test
	public void testregexBond2 () throws Exception{
		assertFalse(jBondTest.regexBond("( ( ("));
	}

	@Test
	public void testregexBond3 () throws Exception{
		assertFalse(jBondTest.regexBond("( ( )"));
	}

	@Test
	public void testregexBond4 () throws Exception{
		assertTrue(jBondTest.regexBond("( ( 0 0 7 )"));
	}

	@Test
	public void testregexBond5 () throws Exception{
		assertFalse(jBondTest.regexBond("( ( 0 7 )"));
	}

	@Test
	public void testregexBond6 () throws Exception{
		assertFalse(jBondTest.regexBond("( ( 7 )"));
	}

	@Test
	public void testregexBond7 () throws Exception{
		assertFalse(jBondTest.regexBond("( )"));
	}

	@Test
	public void testregexBond8 () throws Exception{
		assertFalse(jBondTest.regexBond("( ) ("));
	}

	@Test
	public void testregexBond9 () throws Exception{
		assertFalse(jBondTest.regexBond("( ) )"));
	}

	@Test
	public void testregexBond10 () throws Exception{
		assertTrue(jBondTest.regexBond("( ) 0 0 7 )"));
	}

	@Test
	public void testregexBond11 () throws Exception{
		assertFalse(jBondTest.regexBond("( ) 0 7 )"));
	}

	@Test
	public void testregexBond12 () throws Exception{
		assertFalse(jBondTest.regexBond("( ) 7 )"));
	}

	@Test
	public void testregexBond13 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 ("));
	}

	@Test
	public void testregexBond14 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 ( ("));
	}

	@Test
	public void testregexBond15 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 ( )"));
	}

	@Test
	public void testregexBond16 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 ( 0 0 7 )"));
	}

	@Test
	public void testregexBond17 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 ( 0 7 )"));
	}

	@Test
	public void testregexBond18 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 ( 7 )"));
	}

	@Test
	public void testregexBond19 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 )"));
	}

	@Test
	public void testregexBond20 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 ) ("));
	}

	@Test
	public void testregexBond21 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 ) )"));
	}

	@Test
	public void testregexBond22 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 ) 0 0 7 )"));
	}

	@Test
	public void testregexBond23 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 ) 0 7 )"));
	}

	@Test
	public void testregexBond24 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 ) 7 )"));
	}

	@Test
	public void testregexBond25 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 ("));
	}

	@Test
	public void testregexBond26 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 ( ("));
	}

	@Test
	public void testregexBond27 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 ( )"));
	}

	@Test
	public void testregexBond28 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 ( 0 0 7 )"));
	}

	@Test
	public void testregexBond29 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 ( 0 7 )"));
	}

	@Test
	public void testregexBond30 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 ( 7 )"));
	}

	@Test
	public void testregexBond31 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 )"));
	}

	@Test
	public void testregexBond32 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 ) ("));
	}

	@Test
	public void testregexBond33 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 ) )"));
	}

	@Test
	public void testregexBond34 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 ) 0 0 7 )"));
	}

	@Test
	public void testregexBond35 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 ) 0 7 )"));
	}

	@Test
	public void testregexBond36 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 ) 7 )"));
	}

	@Test
	public void testregexBond37 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 0 ("));
	}

	@Test
	public void testregexBond38 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 0 )"));
	}

	@Test
	public void testregexBond39 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 0 0 0 7 )"));
	}

	@Test
	public void testregexBond40 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 0 0 7 )"));
	}

	@Test
	public void testregexBond41 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 0 7 )"));
	}

	@Test
	public void testregexBond42 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 1 ("));
	}

	@Test
	public void testregexBond43 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 1 )"));
	}

	@Test
	public void testregexBond44 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 1 0 0 7 )"));
	}

	@Test
	public void testregexBond45 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 1 0 7 )"));
	}

	@Test
	public void testregexBond46 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 1 7 )"));
	}

	@Test
	public void testregexBond47 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 2 ("));
	}

	@Test
	public void testregexBond48 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 2 )"));
	}

	@Test
	public void testregexBond49 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 2 0 0 7 )"));
	}

	@Test
	public void testregexBond50 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 2 0 7 )"));
	}

	@Test
	public void testregexBond51 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 2 7 )"));
	}

	@Test
	public void testregexBond52 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 3 ("));
	}

	@Test
	public void testregexBond53 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 3 )"));
	}

	@Test
	public void testregexBond54 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 3 0 0 7 )"));
	}

	@Test
	public void testregexBond55 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 3 0 7 )"));
	}

	@Test
	public void testregexBond56 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 3 7 )"));
	}

	@Test
	public void testregexBond57 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 4 ("));
	}

	@Test
	public void testregexBond58 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 4 )"));
	}

	@Test
	public void testregexBond59 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 4 0 0 7 )"));
	}

	@Test
	public void testregexBond60 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 4 0 7 )"));
	}

	@Test
	public void testregexBond61 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 4 7 )"));
	}

	@Test
	public void testregexBond62 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 5 ("));
	}

	@Test
	public void testregexBond63 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 5 )"));
	}

	@Test
	public void testregexBond64 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 5 0 0 7 )"));
	}

	@Test
	public void testregexBond65 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 5 0 7 )"));
	}

	@Test
	public void testregexBond66 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 5 7 )"));
	}

	@Test
	public void testregexBond67 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 6 ("));
	}

	@Test
	public void testregexBond68 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 6 )"));
	}

	@Test
	public void testregexBond69 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 6 0 0 7 )"));
	}

	@Test
	public void testregexBond70 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 6 0 7 )"));
	}

	@Test
	public void testregexBond71 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 6 7 )"));
	}

	@Test
	public void testregexBond72 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 7 ("));
	}

	@Test
	public void testregexBond73 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 7 ( ("));
	}

	@Test
	public void testregexBond74 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ( )"));
	}

	@Test
	public void testregexBond75 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ( 0 0 7 )"));
	}

	@Test
	public void testregexBond76 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ( 0 7 )"));
	}

	@Test
	public void testregexBond77 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ( 7 )"));
	}

	@Test
	public void testregexBond78 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 )"));
	}

	@Test
	public void testregexBond79 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) ("));
	}

	@Test
	public void testregexBond80 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) ( ("));
	}

	@Test
	public void testregexBond81 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) ( )"));
	}

	@Test
	public void testregexBond82 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) ( 0 0 7 )"));
	}

	@Test
	public void testregexBond83 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) ( 0 7 )"));
	}

	@Test
	public void testregexBond84 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) ( 7 )"));
	}

	@Test
	public void testregexBond85 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) )"));
	}

	@Test
	public void testregexBond86 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) ) ("));
	}

	@Test
	public void testregexBond87 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) ) )"));
	}

	@Test
	public void testregexBond88 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) ) 0 0 7 )"));
	}

	@Test
	public void testregexBond89 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) ) 0 7 )"));
	}

	@Test
	public void testregexBond90 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) ) 7 )"));
	}

	@Test
	public void testregexBond91 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 0 ("));
	}

	@Test
	public void testregexBond92 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 0 )"));
	}

	@Test
	public void testregexBond93 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 0 0 0 7 )"));
	}

	@Test
	public void testregexBond94 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 0 0 7 )"));
	}

	@Test
	public void testregexBond95 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 0 7 )"));
	}

	@Test
	public void testregexBond96 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 1 ("));
	}

	@Test
	public void testregexBond97 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 1 )"));
	}

	@Test
	public void testregexBond98 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 1 0 0 7 )"));
	}

	@Test
	public void testregexBond99 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 1 0 7 )"));
	}

	@Test
	public void testregexBond100 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 1 7 )"));
	}

	@Test
	public void testregexBond101 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 2 ("));
	}

	@Test
	public void testregexBond102 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 2 )"));
	}

	@Test
	public void testregexBond103 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 2 0 0 7 )"));
	}

	@Test
	public void testregexBond104 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 2 0 7 )"));
	}

	@Test
	public void testregexBond105 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 2 7 )"));
	}

	@Test
	public void testregexBond106 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 3 ("));
	}

	@Test
	public void testregexBond107 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 3 )"));
	}

	@Test
	public void testregexBond108 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 3 0 0 7 )"));
	}

	@Test
	public void testregexBond109 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 3 0 7 )"));
	}

	@Test
	public void testregexBond110 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 3 7 )"));
	}

	@Test
	public void testregexBond111 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 4 ("));
	}

	@Test
	public void testregexBond112 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 4 )"));
	}

	@Test
	public void testregexBond113 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 4 0 0 7 )"));
	}

	@Test
	public void testregexBond114 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 4 0 7 )"));
	}

	@Test
	public void testregexBond115 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 4 7 )"));
	}

	@Test
	public void testregexBond116 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 5 ("));
	}

	@Test
	public void testregexBond117 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 5 )"));
	}

	@Test
	public void testregexBond118 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 5 0 0 7 )"));
	}

	@Test
	public void testregexBond119 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 5 0 7 )"));
	}

	@Test
	public void testregexBond120 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 5 7 )"));
	}

	@Test
	public void testregexBond121 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 6 ("));
	}

	@Test
	public void testregexBond122 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 6 )"));
	}

	@Test
	public void testregexBond123 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 6 0 0 7 )"));
	}

	@Test
	public void testregexBond124 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 6 0 7 )"));
	}

	@Test
	public void testregexBond125 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 6 7 )"));
	}

	@Test
	public void testregexBond126 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 7 ("));
	}

	@Test
	public void testregexBond127 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 7 )"));
	}

	@Test
	public void testregexBond128 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 7 0 0 7 )"));
	}

	@Test
	public void testregexBond129 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 7 0 7 )"));
	}

	@Test
	public void testregexBond130 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 7 7 )"));
	}

	@Test
	public void testregexBond131 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 8 ("));
	}

	@Test
	public void testregexBond132 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 8 )"));
	}

	@Test
	public void testregexBond133 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 8 0 0 7 )"));
	}

	@Test
	public void testregexBond134 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 8 0 7 )"));
	}

	@Test
	public void testregexBond135 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 8 7 )"));
	}

	@Test
	public void testregexBond136 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 9 ("));
	}

	@Test
	public void testregexBond137 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 9 )"));
	}

	@Test
	public void testregexBond138 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 9 0 0 7 )"));
	}

	@Test
	public void testregexBond139 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 9 0 7 )"));
	}

	@Test
	public void testregexBond140 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 ) 9 7 )"));
	}

	@Test
	public void testregexBond141 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 7 0 ("));
	}

	@Test
	public void testregexBond142 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 0 )"));
	}

	@Test
	public void testregexBond143 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 0 0 0 7 )"));
	}

	@Test
	public void testregexBond144 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 0 0 7 )"));
	}

	@Test
	public void testregexBond145 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 0 7 )"));
	}

	@Test
	public void testregexBond146 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 7 1 ("));
	}

	@Test
	public void testregexBond147 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 1 )"));
	}

	@Test
	public void testregexBond148 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 1 0 0 7 )"));
	}

	@Test
	public void testregexBond149 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 1 0 7 )"));
	}

	@Test
	public void testregexBond150 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 1 7 )"));
	}

	@Test
	public void testregexBond151 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 7 2 ("));
	}

	@Test
	public void testregexBond152 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 2 )"));
	}

	@Test
	public void testregexBond153 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 2 0 0 7 )"));
	}

	@Test
	public void testregexBond154 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 2 0 7 )"));
	}

	@Test
	public void testregexBond155 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 2 7 )"));
	}

	@Test
	public void testregexBond156 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 7 3 ("));
	}

	@Test
	public void testregexBond157 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 3 )"));
	}

	@Test
	public void testregexBond158 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 3 0 0 7 )"));
	}

	@Test
	public void testregexBond159 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 3 0 7 )"));
	}

	@Test
	public void testregexBond160 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 3 7 )"));
	}

	@Test
	public void testregexBond161 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 7 4 ("));
	}

	@Test
	public void testregexBond162 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 4 )"));
	}

	@Test
	public void testregexBond163 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 4 0 0 7 )"));
	}

	@Test
	public void testregexBond164 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 4 0 7 )"));
	}

	@Test
	public void testregexBond165 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 4 7 )"));
	}

	@Test
	public void testregexBond166 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 7 5 ("));
	}

	@Test
	public void testregexBond167 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 5 )"));
	}

	@Test
	public void testregexBond168 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 5 0 0 7 )"));
	}

	@Test
	public void testregexBond169 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 5 0 7 )"));
	}

	@Test
	public void testregexBond170 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 5 7 )"));
	}

	@Test
	public void testregexBond171 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 7 6 ("));
	}

	@Test
	public void testregexBond172 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 6 )"));
	}

	@Test
	public void testregexBond173 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 6 0 0 7 )"));
	}

	@Test
	public void testregexBond174 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 6 0 7 )"));
	}

	@Test
	public void testregexBond175 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 6 7 )"));
	}

	@Test
	public void testregexBond176 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 7 7 ("));
	}

	@Test
	public void testregexBond177 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 7 )"));
	}

	@Test
	public void testregexBond178 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 7 0 0 7 )"));
	}

	@Test
	public void testregexBond179 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 7 0 7 )"));
	}

	@Test
	public void testregexBond180 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 7 7 )"));
	}

	@Test
	public void testregexBond181 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 7 8 ("));
	}

	@Test
	public void testregexBond182 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 8 )"));
	}

	@Test
	public void testregexBond183 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 8 0 0 7 )"));
	}

	@Test
	public void testregexBond184 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 8 0 7 )"));
	}

	@Test
	public void testregexBond185 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 8 7 )"));
	}

	@Test
	public void testregexBond186 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 7 9 ("));
	}

	@Test
	public void testregexBond187 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 9 )"));
	}

	@Test
	public void testregexBond188 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 9 0 0 7 )"));
	}

	@Test
	public void testregexBond189 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 9 0 7 )"));
	}

	@Test
	public void testregexBond190 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 7 9 7 )"));
	}

	@Test
	public void testregexBond191 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 8 ("));
	}

	@Test
	public void testregexBond192 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 8 )"));
	}

	@Test
	public void testregexBond193 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 8 0 0 7 )"));
	}

	@Test
	public void testregexBond194 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 8 0 7 )"));
	}

	@Test
	public void testregexBond195 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 8 7 )"));
	}

	@Test
	public void testregexBond196 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 9 ("));
	}

	@Test
	public void testregexBond197 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 9 )"));
	}

	@Test
	public void testregexBond198 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 0 9 0 0 7 )"));
	}

	@Test
	public void testregexBond199 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 9 0 7 )"));
	}

	@Test
	public void testregexBond200 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 0 9 7 )"));
	}

	@Test
	public void testregexBond201 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 1 ("));
	}

	@Test
	public void testregexBond202 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 1 )"));
	}

	@Test
	public void testregexBond203 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 1 0 0 7 )"));
	}

	@Test
	public void testregexBond204 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 1 0 7 )"));
	}

	@Test
	public void testregexBond205 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 1 7 )"));
	}

	@Test
	public void testregexBond206 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 2 ("));
	}

	@Test
	public void testregexBond207 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 2 )"));
	}

	@Test
	public void testregexBond208 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 2 0 0 7 )"));
	}

	@Test
	public void testregexBond209 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 2 0 7 )"));
	}

	@Test
	public void testregexBond210 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 2 7 )"));
	}

	@Test
	public void testregexBond211 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 3 ("));
	}

	@Test
	public void testregexBond212 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 3 )"));
	}

	@Test
	public void testregexBond213 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 3 0 0 7 )"));
	}

	@Test
	public void testregexBond214 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 3 0 7 )"));
	}

	@Test
	public void testregexBond215 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 3 7 )"));
	}

	@Test
	public void testregexBond216 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 4 ("));
	}

	@Test
	public void testregexBond217 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 4 )"));
	}

	@Test
	public void testregexBond218 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 4 0 0 7 )"));
	}

	@Test
	public void testregexBond219 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 4 0 7 )"));
	}

	@Test
	public void testregexBond220 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 4 7 )"));
	}

	@Test
	public void testregexBond221 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 5 ("));
	}

	@Test
	public void testregexBond222 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 5 )"));
	}

	@Test
	public void testregexBond223 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 5 0 0 7 )"));
	}

	@Test
	public void testregexBond224 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 5 0 7 )"));
	}

	@Test
	public void testregexBond225 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 5 7 )"));
	}

	@Test
	public void testregexBond226 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 6 ("));
	}

	@Test
	public void testregexBond227 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 6 )"));
	}

	@Test
	public void testregexBond228 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 6 0 0 7 )"));
	}

	@Test
	public void testregexBond229 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 6 0 7 )"));
	}

	@Test
	public void testregexBond230 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 6 7 )"));
	}

	@Test
	public void testregexBond231 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 7 ("));
	}

	@Test
	public void testregexBond232 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 7 )"));
	}

	@Test
	public void testregexBond233 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 7 0 0 7 )"));
	}

	@Test
	public void testregexBond234 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 7 0 7 )"));
	}

	@Test
	public void testregexBond235 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 7 7 )"));
	}

	@Test
	public void testregexBond236 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 8 ("));
	}

	@Test
	public void testregexBond237 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 8 )"));
	}

	@Test
	public void testregexBond238 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 8 0 0 7 )"));
	}

	@Test
	public void testregexBond239 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 8 0 7 )"));
	}

	@Test
	public void testregexBond240 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 8 7 )"));
	}

	@Test
	public void testregexBond241 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 9 ("));
	}

	@Test
	public void testregexBond242 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 9 )"));
	}

	@Test
	public void testregexBond243 () throws Exception{
		assertTrue(jBondTest.regexBond("( 0 9 0 0 7 )"));
	}

	@Test
	public void testregexBond244 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 9 0 7 )"));
	}

	@Test
	public void testregexBond245 () throws Exception{
		assertFalse(jBondTest.regexBond("( 0 9 7 )"));
	}

	@Test
	public void testregexBond246 () throws Exception{
		assertFalse(jBondTest.regexBond("( 1 ("));
	}

	@Test
	public void testregexBond247 () throws Exception{
		assertFalse(jBondTest.regexBond("( 1 )"));
	}

	@Test
	public void testregexBond248 () throws Exception{
		assertTrue(jBondTest.regexBond("( 1 0 0 7 )"));
	}

	@Test
	public void testregexBond249 () throws Exception{
		assertFalse(jBondTest.regexBond("( 1 0 7 )"));
	}

	@Test
	public void testregexBond250 () throws Exception{
		assertFalse(jBondTest.regexBond("( 1 7 )"));
	}

	@Test
	public void testregexBond251 () throws Exception{
		assertFalse(jBondTest.regexBond("( 2 ("));
	}

	@Test
	public void testregexBond252 () throws Exception{
		assertFalse(jBondTest.regexBond("( 2 )"));
	}

	@Test
	public void testregexBond253 () throws Exception{
		assertTrue(jBondTest.regexBond("( 2 0 0 7 )"));
	}

	@Test
	public void testregexBond254 () throws Exception{
		assertFalse(jBondTest.regexBond("( 2 0 7 )"));
	}

	@Test
	public void testregexBond255 () throws Exception{
		assertFalse(jBondTest.regexBond("( 2 7 )"));
	}

	@Test
	public void testregexBond256 () throws Exception{
		assertFalse(jBondTest.regexBond("( 3 ("));
	}

	@Test
	public void testregexBond257 () throws Exception{
		assertFalse(jBondTest.regexBond("( 3 )"));
	}

	@Test
	public void testregexBond258 () throws Exception{
		assertTrue(jBondTest.regexBond("( 3 0 0 7 )"));
	}

	@Test
	public void testregexBond259 () throws Exception{
		assertFalse(jBondTest.regexBond("( 3 0 7 )"));
	}

	@Test
	public void testregexBond260 () throws Exception{
		assertFalse(jBondTest.regexBond("( 3 7 )"));
	}

	@Test
	public void testregexBond261 () throws Exception{
		assertFalse(jBondTest.regexBond("( 4 ("));
	}

	@Test
	public void testregexBond262 () throws Exception{
		assertFalse(jBondTest.regexBond("( 4 )"));
	}

	@Test
	public void testregexBond263 () throws Exception{
		assertTrue(jBondTest.regexBond("( 4 0 0 7 )"));
	}

	@Test
	public void testregexBond264 () throws Exception{
		assertFalse(jBondTest.regexBond("( 4 0 7 )"));
	}

	@Test
	public void testregexBond265 () throws Exception{
		assertFalse(jBondTest.regexBond("( 4 7 )"));
	}

	@Test
	public void testregexBond266 () throws Exception{
		assertFalse(jBondTest.regexBond("( 5 ("));
	}

	@Test
	public void testregexBond267 () throws Exception{
		assertFalse(jBondTest.regexBond("( 5 )"));
	}

	@Test
	public void testregexBond268 () throws Exception{
		assertTrue(jBondTest.regexBond("( 5 0 0 7 )"));
	}

	@Test
	public void testregexBond269 () throws Exception{
		assertFalse(jBondTest.regexBond("( 5 0 7 )"));
	}

	@Test
	public void testregexBond270 () throws Exception{
		assertFalse(jBondTest.regexBond("( 5 7 )"));
	}

	@Test
	public void testregexBond271 () throws Exception{
		assertFalse(jBondTest.regexBond("( 6 ("));
	}

	@Test
	public void testregexBond272 () throws Exception{
		assertFalse(jBondTest.regexBond("( 6 )"));
	}

	@Test
	public void testregexBond273 () throws Exception{
		assertTrue(jBondTest.regexBond("( 6 0 0 7 )"));
	}

	@Test
	public void testregexBond274 () throws Exception{
		assertFalse(jBondTest.regexBond("( 6 0 7 )"));
	}

	@Test
	public void testregexBond275 () throws Exception{
		assertFalse(jBondTest.regexBond("( 6 7 )"));
	}

	@Test
	public void testregexBond276 () throws Exception{
		assertFalse(jBondTest.regexBond("( 7 ("));
	}

	@Test
	public void testregexBond277 () throws Exception{
		assertFalse(jBondTest.regexBond("( 7 )"));
	}

	@Test
	public void testregexBond278 () throws Exception{
		assertTrue(jBondTest.regexBond("( 7 0 0 7 )"));
	}

	@Test
	public void testregexBond279 () throws Exception{
		assertFalse(jBondTest.regexBond("( 7 0 7 )"));
	}

	@Test
	public void testregexBond280 () throws Exception{
		assertFalse(jBondTest.regexBond("( 7 7 )"));
	}

	@Test
	public void testregexBond281 () throws Exception{
		assertFalse(jBondTest.regexBond("( 8 ("));
	}

	@Test
	public void testregexBond282 () throws Exception{
		assertFalse(jBondTest.regexBond("( 8 )"));
	}

	@Test
	public void testregexBond283 () throws Exception{
		assertTrue(jBondTest.regexBond("( 8 0 0 7 )"));
	}

	@Test
	public void testregexBond284 () throws Exception{
		assertFalse(jBondTest.regexBond("( 8 0 7 )"));
	}

	@Test
	public void testregexBond285 () throws Exception{
		assertFalse(jBondTest.regexBond("( 8 7 )"));
	}

	@Test
	public void testregexBond286 () throws Exception{
		assertFalse(jBondTest.regexBond("( 9 ("));
	}

	@Test
	public void testregexBond287 () throws Exception{
		assertFalse(jBondTest.regexBond("( 9 )"));
	}

	@Test
	public void testregexBond288 () throws Exception{
		assertTrue(jBondTest.regexBond("( 9 0 0 7 )"));
	}

	@Test
	public void testregexBond289 () throws Exception{
		assertFalse(jBondTest.regexBond("( 9 0 7 )"));
	}

	@Test
	public void testregexBond290 () throws Exception{
		assertFalse(jBondTest.regexBond("( 9 7 )"));
	}

	@Test
	public void testregexBond291 () throws Exception{
		assertFalse(jBondTest.regexBond(")"));
	}

	@Test
	public void testregexBond292 () throws Exception{
		assertFalse(jBondTest.regexBond(") ("));
	}

	@Test
	public void testregexBond293 () throws Exception{
		assertFalse(jBondTest.regexBond(") )"));
	}

	@Test
	public void testregexBond294 () throws Exception{
		assertFalse(jBondTest.regexBond(") 0 0 7 )"));
	}

	@Test
	public void testregexBond295 () throws Exception{
		assertFalse(jBondTest.regexBond(") 0 7 )"));
	}

	@Test
	public void testregexBond296 () throws Exception{
		assertFalse(jBondTest.regexBond(") 7 )"));
	}

	@Test
	public void testregexBond297 () throws Exception{
		assertFalse(jBondTest.regexBond("0 ("));
	}

	@Test
	public void testregexBond298 () throws Exception{
		assertFalse(jBondTest.regexBond("0 )"));
	}

	@Test
	public void testregexBond299 () throws Exception{
		assertFalse(jBondTest.regexBond("0 0 0 7 )"));
	}

	@Test
	public void testregexBond300 () throws Exception{
		assertFalse(jBondTest.regexBond("0 0 7 )"));
	}

	@Test
	public void testregexBond301 () throws Exception{
		assertFalse(jBondTest.regexBond("0 7 )"));
	}

	@Test
	public void testregexBond302 () throws Exception{
		assertFalse(jBondTest.regexBond("1 ("));
	}

	@Test
	public void testregexBond303 () throws Exception{
		assertFalse(jBondTest.regexBond("1 )"));
	}

	@Test
	public void testregexBond304 () throws Exception{
		assertFalse(jBondTest.regexBond("1 0 0 7 )"));
	}

	@Test
	public void testregexBond305 () throws Exception{
		assertFalse(jBondTest.regexBond("1 0 7 )"));
	}

	@Test
	public void testregexBond306 () throws Exception{
		assertFalse(jBondTest.regexBond("1 7 )"));
	}

	@Test
	public void testregexBond307 () throws Exception{
		assertFalse(jBondTest.regexBond("2 ("));
	}

	@Test
	public void testregexBond308 () throws Exception{
		assertFalse(jBondTest.regexBond("2 )"));
	}

	@Test
	public void testregexBond309 () throws Exception{
		assertFalse(jBondTest.regexBond("2 0 0 7 )"));
	}

	@Test
	public void testregexBond310 () throws Exception{
		assertFalse(jBondTest.regexBond("2 0 7 )"));
	}

	@Test
	public void testregexBond311 () throws Exception{
		assertFalse(jBondTest.regexBond("2 7 )"));
	}

	@Test
	public void testregexBond312 () throws Exception{
		assertFalse(jBondTest.regexBond("3 ("));
	}

	@Test
	public void testregexBond313 () throws Exception{
		assertFalse(jBondTest.regexBond("3 )"));
	}

	@Test
	public void testregexBond314 () throws Exception{
		assertFalse(jBondTest.regexBond("3 0 0 7 )"));
	}

	@Test
	public void testregexBond315 () throws Exception{
		assertFalse(jBondTest.regexBond("3 0 7 )"));
	}

	@Test
	public void testregexBond316 () throws Exception{
		assertFalse(jBondTest.regexBond("3 7 )"));
	}

	@Test
	public void testregexBond317 () throws Exception{
		assertFalse(jBondTest.regexBond("4 ("));
	}

	@Test
	public void testregexBond318 () throws Exception{
		assertFalse(jBondTest.regexBond("4 )"));
	}

	@Test
	public void testregexBond319 () throws Exception{
		assertFalse(jBondTest.regexBond("4 0 0 7 )"));
	}

	@Test
	public void testregexBond320 () throws Exception{
		assertFalse(jBondTest.regexBond("4 0 7 )"));
	}

	@Test
	public void testregexBond321 () throws Exception{
		assertFalse(jBondTest.regexBond("4 7 )"));
	}

	@Test
	public void testregexBond322 () throws Exception{
		assertFalse(jBondTest.regexBond("5 ("));
	}

	@Test
	public void testregexBond323 () throws Exception{
		assertFalse(jBondTest.regexBond("5 )"));
	}

	@Test
	public void testregexBond324 () throws Exception{
		assertFalse(jBondTest.regexBond("5 0 0 7 )"));
	}

	@Test
	public void testregexBond325 () throws Exception{
		assertFalse(jBondTest.regexBond("5 0 7 )"));
	}

	@Test
	public void testregexBond326 () throws Exception{
		assertFalse(jBondTest.regexBond("5 7 )"));
	}

	@Test
	public void testregexBond327 () throws Exception{
		assertFalse(jBondTest.regexBond("6 ("));
	}

	@Test
	public void testregexBond328 () throws Exception{
		assertFalse(jBondTest.regexBond("6 )"));
	}

	@Test
	public void testregexBond329 () throws Exception{
		assertFalse(jBondTest.regexBond("6 0 0 7 )"));
	}

	@Test
	public void testregexBond330 () throws Exception{
		assertFalse(jBondTest.regexBond("6 0 7 )"));
	}

	@Test
	public void testregexBond331 () throws Exception{
		assertFalse(jBondTest.regexBond("6 7 )"));
	}

	@Test
	public void testregexBond332 () throws Exception{
		assertFalse(jBondTest.regexBond("7 ("));
	}

	@Test
	public void testregexBond333 () throws Exception{
		assertFalse(jBondTest.regexBond("7 )"));
	}

	@Test
	public void testregexBond334 () throws Exception{
		assertFalse(jBondTest.regexBond("7 0 0 7 )"));
	}

	@Test
	public void testregexBond335 () throws Exception{
		assertFalse(jBondTest.regexBond("7 0 7 )"));
	}

	@Test
	public void testregexBond336 () throws Exception{
		assertFalse(jBondTest.regexBond("7 7 )"));
	}

	@Test
	public void testregexBond337 () throws Exception{
		assertFalse(jBondTest.regexBond("8 ("));
	}

	@Test
	public void testregexBond338 () throws Exception{
		assertFalse(jBondTest.regexBond("8 )"));
	}

	@Test
	public void testregexBond339 () throws Exception{
		assertFalse(jBondTest.regexBond("8 0 0 7 )"));
	}

	@Test
	public void testregexBond340 () throws Exception{
		assertFalse(jBondTest.regexBond("8 0 7 )"));
	}

	@Test
	public void testregexBond341 () throws Exception{
		assertFalse(jBondTest.regexBond("8 7 )"));
	}

	@Test
	public void testregexBond342 () throws Exception{
		assertFalse(jBondTest.regexBond("9 ("));
	}

	@Test
	public void testregexBond343 () throws Exception{
		assertFalse(jBondTest.regexBond("9 )"));
	}

	@Test
	public void testregexBond344 () throws Exception{
		assertFalse(jBondTest.regexBond("9 0 0 7 )"));
	}

	@Test
	public void testregexBond345 () throws Exception{
		assertFalse(jBondTest.regexBond("9 0 7 )"));
	}

	@Test
	public void testregexBond346 () throws Exception{
		assertFalse(jBondTest.regexBond("9 7 )"));
	}



}
