package Project5;

public class JamesBond {

	public void JamesBond() {

	}

	public boolean regexBond(String testCaseInput) {
		String bondRegexString = ".*[(].*(007).*[)].*";
		
		testCaseInput = testCaseInput.replaceAll("\\s", "");
		
		boolean returnVal = testCaseInput.matches(bondRegexString);
		
		return returnVal;
	}

}
